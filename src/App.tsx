import React from 'react';
import './App.css';
import BerlinClock from "./BerlinClock";

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <p>
                    Berlin Clock - A <em>programming kata</em>
                </p>
                <BerlinClock />
                <p><a href="http://agilekatas.co.uk/katas/BerlinClock-Kata">Kata text</a></p>
            </header>
        </div>
    );
}

export default App;
